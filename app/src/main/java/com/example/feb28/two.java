package com.example.feb28;

import android.content.Intent;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.TimeUtils;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import android.support.design.widget.FloatingActionButton;


import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.sql.Time;
import java.util.concurrent.TimeUnit;

import androidx.work.Constraints;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

public class two extends AppCompatActivity {
    private FirebaseAuth mAuth;
    GoogleSignInClient mGoogleSignInClient;
    GoogleApiClient mGoogleApiClient;
    FirebaseAuth.AuthStateListener authStateListener;
    Button submit,stop,periodic;
    GpsLocation gps=new GpsLocation(two.this);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_two);
        submit=(Button)findViewById(R.id.submit);
        periodic=(Button)findViewById(R.id.periodic);
        stop=(Button)findViewById(R.id.stop);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                submitbutton();

            }
        });
        final Constraints periodicconstraints=new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED ).build();



        periodic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PeriodicWorkRequest refreshWork =
                        new PeriodicWorkRequest.Builder(MyWorker.class, 300000, TimeUnit.MILLISECONDS)
                                .addTag("periodic request" )
                                .setConstraints(periodicconstraints)
                                .build();
                WorkManager.getInstance().enqueue(refreshWork);

            }
        });
        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopbutton();
            }
        });

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fabclick();

            }
        });





    }

    private void submitbutton() {
            /*if(gps.canGetLocation())
            {
                Double latitude = gps.getLatitude();
                Double longitude=gps.getLongitude();
                Toast.makeText(this,latitude+longitude,Toast.LENGTH_SHORT).show();
            }
            else
            {
                Log.d("TAG","not working in the my worker" );
            }*/
        final Constraints singleconstraints=new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.UNMETERED).build();

        final OneTimeWorkRequest simpleRequest = new OneTimeWorkRequest.Builder(MyWorker.class)
                .addTag("lat and long working" )
                .setConstraints(singleconstraints)
                .build();


        WorkManager.getInstance().enqueue(simpleRequest);
    }

    private void stopbutton() {
        WorkManager.getInstance().cancelAllWork();
        Toast.makeText(this,"workmanager stopped ",Toast.LENGTH_SHORT).show();

    }

    private void fabclick() {
        mAuth.signOut();
        Intent intent= new Intent(two.this,MainActivity.class);
        intent.addFlags(intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
}
