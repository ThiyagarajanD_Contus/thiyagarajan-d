package com.example.feb28;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.text.format.Time;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import androidx.work.Operation;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

public class MyWorker extends Worker {

    //GpsLocation gps =new GpsLocation(getApplicationContext()) ;

    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;

    public MyWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {



        mDatabase = FirebaseDatabase.getInstance().getReference();

   FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference mRef =  database.getReference().child("Users").push();
//        FirebaseUser user =  mAuth.getCurrentUser();
  //      String userId = user.getUid();
        Double latitude=10.12;
        Double longitude=131.12;

        mRef.child("User").child("latitude").setValue(latitude);
        mRef.child("User").child("longitude").setValue(longitude);
        Log.d("TAG","lat and long work is dng" );
        sendNotification(latitude,longitude);








        return Result.SUCCESS;
    }

    public void  sendNotification( Double latitude, Double longitude)
    {
        NotificationManager notificationManager=(NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);


        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("default", "Default", NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }


        NotificationCompat.Builder notification = new NotificationCompat.Builder(getApplicationContext(), "default")
                .setContentTitle(latitude.toString()+"  "+longitude.toString())
                .setSmallIcon(R.mipmap.ic_launcher);

        notificationManager.notify(1, notification.build());


    }


    interface w{
        public void sendNotification(String name,int time);

    }
    public class address {

        public double latitude;
        public double longitude;

        public address() {
            // Default constructor required for calls to DataSnapshot.getValue(User.class)
        }

        public address(double latitude, double longitude) {
            this.latitude = latitude;
            this.longitude = longitude;
        }
    }

}
